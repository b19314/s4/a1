package com.zuitt.batch193;

public class Course {

    private String name;
    private String description;
    private int seats;
    private double fee;
    private String startDate;
    private String endDate;

    private String instructorName;

    public Course(){};

    public Course(String name, String description, int seats, double fee, String startDate, String endDate, String instructorName){
        this.name = name;
        this.description = description;
        this.seats = seats;
        this.fee = fee;
        this.startDate = startDate;
        this.endDate = endDate;
        this.instructorName = instructorName;
    }

    public String getName(){
        return this.name;
    }
    public String getDescription(){
        return this.description;
    }
    public int getSeats(){
        return this.seats;
    }
    public double getFee(){
        return this.fee;
    }
    public String getStartDate(){
        return this.startDate;
    }
    public String getEndDate(){
        return this.endDate;
    }
    public String getInstructorName(){
        return this.instructorName;
    }

    public void setName(String name){
        this.name = name;
    }
    public void setDescription(String description){
        this.description = description;
    }

    public void setFee(int fee){
        this.fee = fee;
    }
    public void setSeats(int seats){
        this.seats = seats;
    }
    public void setStartDate(String startDate){
        this.startDate = startDate;
    }
    public void setEndDate(String endDate){
        this.endDate = endDate;
    }
    public void setInstructorName(String instructorName){
        this.instructorName = instructorName;
    }


    public void courseDetails(){
        System.out.println("Course's name:");
        System.out.println(getName());
        System.out.println("Course's description:");
        System.out.println(getDescription());
        System.out.println("Course's seats:");
        System.out.println(getSeats());
        System.out.println("Course's instructor's name:");
        System.out.println(getInstructorName());
    }



}
