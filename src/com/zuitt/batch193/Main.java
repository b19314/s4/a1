package com.zuitt.batch193;

public class Main {

    public static void main(String[] args){

        User user1 = new User("Vic", "Manuel", 33, "Cainta, Rizal");

        user1.details();

        Course newCourse = new Course();

        newCourse.setName("IT");
        newCourse.setDescription("Students will knowledge of computer concepts  ");
        newCourse.setFee(10000);
        newCourse.setSeats(20);
        newCourse.setStartDate("June 6, 2022");
        newCourse.setEndDate("March 25, 2023");
        newCourse.setInstructorName("Mr. Jimmy");

        newCourse.courseDetails();


    }


}


