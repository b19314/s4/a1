package com.zuitt.batch193;

public class User {

    private String firstName;
    private String lastName;
    private int age;
    private String address;

    public User(){};

    public User(String firstName, String lastName, int age, String address){
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.address = address;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getAge() {
        return age;
    }

    public String getAddress() {
        return address;
    }

    public void details() {
        System.out.println("User's first name: ");
        System.out.println(getFirstName());
        System.out.println("User's last name: ");
        System.out.println(getLastName());
        System.out.println("User's age: ");
        System.out.println(getAge());
        System.out.println("User's address");
        System.out.println(getAddress());
    }

}
